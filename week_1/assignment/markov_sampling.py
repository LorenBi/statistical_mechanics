from random import uniform
from math import pi, sqrt
import pylab as pl


def markov_pi(N, delta):
    x, y = 1.0, 1.0
    n_hits = 0
    for i in range(N):
        del_x, del_y = uniform(-delta, delta), uniform(-delta, delta)
        if abs(x + del_x) < 1.0 and abs(y + del_y) < 1.0:
            x, y = x + del_x, y + del_y
        if (x ** 2 + y ** 2) < 1.0:
            n_hits += 1
    return n_hits


n_runs = 500
for delta in [0.062, 0.125, 0.25, 0.5, 1.0, 2.0, 4.0]:
    n_trials_list = []
    sigmas = []
    print 'Calculation with delta =', delta, '\n'
    for poweroftwo in range(4, 13):
        print 'Calculating with n_trials = 2 ** ' + str(poweroftwo)

        n_trials = 2 ** poweroftwo
        sigma = 0.0
        for run in range(n_runs):
            pi_est = 4.0 * markov_pi(n_trials, delta) / float(n_trials)
            sigma += (pi_est - pi) ** 2
        sigmas.append(sqrt(sigma / (n_runs)))
        n_trials_list.append(n_trials)
    print '\n \n'
    pl.plot(n_trials_list, sigmas, 'o', ms=8, label='$\delta = $' + str(delta))

pl.xscale('log')
pl.yscale('log')
pl.xlabel('number of trials')
pl.ylabel('root mean square deviation')
pl.plot([10, 10000], [1.642 / sqrt(10.0),
        1.642 / sqrt(10000.0)], label='direct')
title = 'Markov-chain sampling of pi: root mean square deviation vs. n_trials'
pl.title(title)
pl.legend(loc='upper right')
pl.savefig('markov_sampling_rms_deviation.png')
pl.show()

# QUESTION
# Which of the values of delta gives the most precise results?
# ANSWER
# Delta = 1.0

# QUESTION
# Explain why VERY small values of delta and VERY large values of delta yield a
# less precise result than intermediate values.
# ANSWER
# We expect from delta too large to make the acceptance rate too low biasing
# the result.
# We expect for delta too small to have more hits around the starting point and
# have a big acceptance rate thus biasing the result.
# So if we expect a large error (compared to pi) for small and big delta, we
# naturally expect the function to have a minimum for an intermediate point.

# QUESTION
# Explain in a few words why the error is larger than for the direct sampling
# algorithm, even for the optimal value of delta.
# ANSWER
# Because there is a correlation in the data. This can be corrected by using
# the bunching method.
