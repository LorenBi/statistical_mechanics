from random import uniform
from math import pi, sqrt
import pl


def direct_pi(N):
    n_hits = 0
    for i in range(N):
        x, y = uniform(-1.0, 1.0), uniform(-1.0, 1.0)
        if (x ** 2 + y ** 2) < 1:
            n_hits += 1
    return n_hits


n_runs = 500
rms_for_trials = []
trials = []
for to_the_ith in range(4, 13):
    print 'Calculating with n_trials = 2 ** ' + str(to_the_ith)
    n_trials = 2 ** to_the_ith
    squared_delta = 0.0
    for run in range(n_runs):
        pi_est = 4 * direct_pi(n_trials) / float(n_trials)
        squared_delta += (pi_est - pi) ** 2
    rms_for_trials.append(sqrt(squared_delta / n_runs))
    trials.append(n_trials)

pl.plot(trials, rms_for_trials, 'o')
pl.plot([10.0, 10000.0], [1.642 / sqrt(10.0), 1.642 / sqrt(10000.0)])
pl.xscale('log')
pl.yscale('log')
pl.xlabel('number of trials')
pl.ylabel('root mean square deviation')
pl.title('Direct sampling of pi: root mean square deviation vs. n_trials')
pl.savefig('direct_sampling_rms_deviation.png')
pl.show()


# QUESTION
# Explain in a few words why one can say that the "error" of the direct_pi
# calculation goes like 1.642 / sqrt(N_trials) (maybe explain what that means
# at n_trials = 100). Does the "error" of the direct-sampling algorithm go to
# zero as N_trials goes to infinity?

# ANSWER
# From the plot we can see that the coefficient of the best line that fits the
# is more or less equal to 1.642 / sqrt(N_trials), so we could deduce
# empirically that 1.642 / sqrt(N_trials) is the function that we could've
# found if we made the calculation for pi_est in dependace with the number of
# trials.
# If the curve that destribe the error of pi_est is 1.642 / sqrt(N_trials), we
# can expect an error of 0.1642 for N_trials = 100
# The error goes like 1.642 / sqrt(N_trials) because in the log log plot they
# have the same slope and asitotically they behave the same. If we bring
# N_trials to infinity we expect the error to go to 0.
