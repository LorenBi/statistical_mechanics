from random import uniform


def markov_acceptance(N, delta):
    x, y = 1.0, 1.0
    n_accepted = 0
    for i in range(N):
        del_x, del_y = uniform(-delta, delta), uniform(-delta, delta)
        if abs(x + del_x) < 1.0 and abs(y + del_y) < 1.0:
            x, y = x + del_x, y + del_y
            n_accepted += 1

    return n_accepted


n_runs = 500
for delta in [0.062, 0.125, 0.25, 0.5, 1.0, 2.0, 4.0]:
    sigmas = []
    n_trial = 2 ** 12
    acceptance_rate = markov_acceptance(n_trial, delta) / float(n_trial)
    print 'The acceptance rate with delta =', delta, 'is', acceptance_rate


# delta | acceptance rate
# 0.062 | 0.979
# 0.125 | 0.961
# 0.25  | 0.871
# 0.5   | 0.761
# 1.0   | 0.560
# 2.0   | 0.251
# 4.0   | 0.061
